﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace fakepeopleparser
{
    class Requests
    {
        public static void SetComboboxItems(System.Windows.Forms.ComboBox curComboBox, string[,] inputArr)
        {
            int rows = inputArr.GetUpperBound(0) + 1;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        curComboBox.Items.Add(inputArr[i, j]);
                    }
                }
            }
        }

        public static string FindValueInArray(string [,] InputArray, string Value)
        {
            for (int i = 0; i < InputArray.GetUpperBound(0) + 1; i++)
            {
                if (InputArray[i, 0] == Value)
                {
                    Value = InputArray[i, 1];
                }
            }
            return Value;
        }

        public struct FakePeople
        {
            public string Name;
            public string Adress;
            public string Coordinates;
            public string Phone;
            public DateTime Birthday;
            public string Email;
            public string Company;
            public string Occupation;
            public float Height;
            public float Weight;
        }

        private static string ParseParameter(string inputStr, string beginRemove, 
            string beginParameter, string endParameter)
        {
            string valueParameter = string.Empty;
            int beginWordPos = inputStr.IndexOf(beginRemove);
            inputStr = inputStr.Remove(0, beginWordPos);
            beginWordPos = inputStr.IndexOf(beginRemove);
            int endWord = inputStr.IndexOf(endParameter);
            int beginParameterPos = inputStr.IndexOf(beginParameter);
            int beginParameterLength = beginParameter.Length;
            
            valueParameter = inputStr.Substring(
                beginParameterPos + beginParameterLength, endWord - (beginParameterPos + beginParameterLength));

            return valueParameter;
        }

        private static FakePeople ParseResponse(string Response)
        {
            FakePeople fakePerson = new FakePeople();
            string birthday = string.Empty;
            string height = string.Empty;
            string weight = string.Empty;

            fakePerson.Name = ParseParameter(Response, "<div class=\"address\">", "<h3>", "</h3>");
            fakePerson.Adress = ParseParameter(Response, "<div class=\"address\">", "</h3>", "</div>");
            fakePerson.Adress = fakePerson.Adress.Replace("<div class=\"adr\">", String.Empty);
            fakePerson.Adress = fakePerson.Adress.Replace("<br />", Constants.cSpace);
            fakePerson.Adress = fakePerson.Adress.Replace("\n", Constants.cSpace);
            fakePerson.Adress = fakePerson.Adress.Trim();

            fakePerson.Coordinates = ParseParameter(Response, "<dt>Geo coordinates</dt>", "<a id=\"geo\" href=\"javascript:void(0)\">", "</a>");
            fakePerson.Phone = ParseParameter(Response, "<dt>Phone</dt>", "<dd>", "</dd>");
            birthday = ParseParameter(Response, "<dt>Birthday</dt>", "<dd>", "</dd>");
            fakePerson.Birthday = DateTime.Parse(birthday);
            fakePerson.Email = ParseParameter(Response, "<dt>Email Address</dt>", "<dd>", "<div class=\"adtl\">").Trim();
            fakePerson.Company = ParseParameter(Response, "<dt>Company</dt>", "<dd>", "</dd>");
            fakePerson.Occupation = ParseParameter(Response, "<dt>Occupation</dt>", "<dd>", "</dd>");
            
            height = ParseParameter(Response, "<dt>Height</dt>", "(", " centimeters");
            height = height.Replace(".", ",");
            fakePerson.Height = float.Parse(height);
            weight = ParseParameter(Response, "<dt>Weight</dt>", "(", " kilograms");
            weight = weight.Replace(".", ",");
            fakePerson.Weight = float.Parse(weight);

            return fakePerson;
        }

        public static string CreateFakePeopleRequest(string gender, string nameset, string state)
        {
            string fakePeopleUrl = String.Empty;
            fakePeopleUrl = Constants.cGetUserUrl;
            var response = String.Empty;
            FakePeople fakePeople = new FakePeople();

            fakePeopleUrl += "-" + gender + "-" + nameset + "-" + state + ".php";

            try
            {
                // Создаём объект WebClient
                using (var webClient = new WebClient())
                {
                    // Выполняем запрос по адресу и получаем ответ в виде строки
                    response = webClient.DownloadString(fakePeopleUrl);
                    //здесь надо распарсить response и вывести в OutputTxb
                    fakePeople = ParseResponse(response);
                    //в столибик поля и значения, примерно, как на самом сайте
                    response = "Name: " + fakePeople.Name + Environment.NewLine +
                        "Adress: " + fakePeople.Adress + Environment.NewLine +
                        "Coordinates: " + fakePeople.Coordinates + Environment.NewLine +
                        "Phone: " + fakePeople.Phone + Environment.NewLine +
                        "Birthday: " + fakePeople.Birthday.ToString() + Environment.NewLine +
                        "Email: " + fakePeople.Email + Environment.NewLine +
                        "Company: " + fakePeople.Company + Environment.NewLine +
                        "Occupation: " + fakePeople.Occupation + Environment.NewLine +
                        "Height: " + fakePeople.Height.ToString() + Environment.NewLine +
                        "Weight: " + fakePeople.Weight.ToString() + Environment.NewLine;

                    DBConnector.SavePeopleInDB(fakePeople);
                };
            }
            catch (Exception e)
            {
                Loger.WriteLogRecord("CreateFakePeopleRequest. Error: " + e.ToString());
            }
            Loger.WriteLogRecord("CreateFakePeopleRequest. Result: Success");
            return response;
        }
    }
}
