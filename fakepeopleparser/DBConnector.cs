﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.Entity;
using MySql.Data.MySqlClient;

namespace fakepeopleparser
{
    class DBConnector
    {
        //подключение к БД
        const string connStr = "server=localhost;user=root;database=fakepeople;port=3306;password=masterkey";
        MySqlConnection conn = new MySqlConnection(connStr);

        public List<string[]> LoadAllRecordsFromDB(List<string[]> data)
        {
            const int ColCount = 11;

            try
            {
                conn.Open();
                //прологировать подключение
                string sql = "SELECT * FROM people";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    data.Add(new string[ColCount]);
                    for (int i = 0; i < ColCount; i++)
                    {
                        data[data.Count - 1][i] = reader[i].ToString();
                    }
                }
                reader.Close();
            }
            catch (Exception e)
            {
                Loger.WriteLogRecord("LoadAllRecordsFromDB. Error: " + e.ToString());
            }

            conn.Close();
            Loger.WriteLogRecord("LoadAllRecordsFromDB. Result: Success");
            return data;
        }

        public static void SavePeopleInDB(Requests.FakePeople CreatePeople)
        {
            try
            {
                MySqlConnection conn = new MySqlConnection(connStr);
                conn.Open();
                string sql = "INSERT INTO people " +
                    "(Name, Adress, Coordinates, Phone, Birthday, Email, Company, Occupation, Height, Weight) " +
                    "VALUES (" +
                    "'" + CreatePeople.Name + "'" + ", " +
                    "'" + CreatePeople.Adress + "'" + ", " +
                    "'" + CreatePeople.Coordinates + "'" + ", " +
                    "'" + CreatePeople.Phone + "'" + ", " +
                    "'" + String.Format("{0:yyyy-MM-dd}", CreatePeople.Birthday) + "'" + ", " +
                    "'" + CreatePeople.Email + "'" + ", " +
                    "'" + CreatePeople.Company + "'" + ", " +
                    "'" + CreatePeople.Occupation + "'" + ", " +
                    "'" + CreatePeople.Height.ToString() + "'" + ", " +
                    "'" + CreatePeople.Weight.ToString().Replace(",",".") + "'" +
                    ")";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception e)
            {
                Loger.WriteLogRecord("SavePeopleInDB. Error: " + e.ToString());
            }
            Loger.WriteLogRecord("SavePeopleInDB. Result: Success");
        }
    }
}
