﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace fakepeopleparser
{
    class Loger
    {
        public static bool IsLocked(string fileName)
        {
            try
            {
                using (FileStream fs = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    fs.Close();
                    // Здесь вызываем свой метод, работаем с файлом
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2147024894)
                    return false;
            }
            return true;
        }

        public static void WriteLogRecord(string Record)
        {
            DateTime NowDate = DateTime.Now;
            Record += Constants.cSpace + NowDate.ToString();
            string LogDirectoryPath = AppDomain.CurrentDomain.BaseDirectory + "/" +
                Constants.cLogsFolder;
            string writePath = LogDirectoryPath + "/" + String.Format("{0:ddMMyyyy}", NowDate) +
                Constants.cFormatLogFile;

            //если папки нет, то создаем её
            if (!Directory.Exists(LogDirectoryPath))
            {
                Directory.CreateDirectory(LogDirectoryPath);
            }

            //если файла нет, то создаем
            if (!File.Exists(writePath))
            {
                //Close нужен, т.к. после Create файл занят другим процессом
                File.Create(writePath).Close(); 
            }

            //дозапись в файл
            while(true)
            {
                if(!IsLocked(writePath))
                {
                    using (StreamWriter Writer = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                    {
                        Writer.WriteLine(Record);
                        break;
                    }
                }            
            }

        }
    }
}
