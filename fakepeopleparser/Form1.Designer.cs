﻿namespace fakepeopleparser
{
    partial class FakePeopleForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.GenBtn = new System.Windows.Forms.Button();
            this.GenderCmb = new System.Windows.Forms.ComboBox();
            this.HeadPanel = new System.Windows.Forms.Panel();
            this.NameSetLbl = new System.Windows.Forms.Label();
            this.NameSetCmb = new System.Windows.Forms.ComboBox();
            this.GenParamsLbl = new System.Windows.Forms.Label();
            this.ReportBtn = new System.Windows.Forms.Button();
            this.LoadPeopleBtn = new System.Windows.Forms.Button();
            this.StateLbl = new System.Windows.Forms.Label();
            this.StateCmb = new System.Windows.Forms.ComboBox();
            this.GenderLbl = new System.Windows.Forms.Label();
            this.FakePeopleGrid = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Adress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Coordinates = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Birthday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Company = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Occupation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Height = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FakePeoplePanel = new System.Windows.Forms.Panel();
            this.OutputTxb = new System.Windows.Forms.TextBox();
            this.OutParamsLbl = new System.Windows.Forms.Label();
            this.HeadPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FakePeopleGrid)).BeginInit();
            this.FakePeoplePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GenBtn
            // 
            this.GenBtn.Location = new System.Drawing.Point(434, 38);
            this.GenBtn.Name = "GenBtn";
            this.GenBtn.Size = new System.Drawing.Size(109, 23);
            this.GenBtn.TabIndex = 0;
            this.GenBtn.Text = "Генерировать";
            this.GenBtn.UseVisualStyleBackColor = true;
            this.GenBtn.Click += new System.EventHandler(this.GenBtn_Click);
            // 
            // GenderCmb
            // 
            this.GenderCmb.FormattingEnabled = true;
            this.GenderCmb.Location = new System.Drawing.Point(13, 40);
            this.GenderCmb.Name = "GenderCmb";
            this.GenderCmb.Size = new System.Drawing.Size(121, 21);
            this.GenderCmb.TabIndex = 1;
            // 
            // HeadPanel
            // 
            this.HeadPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HeadPanel.Controls.Add(this.NameSetLbl);
            this.HeadPanel.Controls.Add(this.NameSetCmb);
            this.HeadPanel.Controls.Add(this.GenParamsLbl);
            this.HeadPanel.Controls.Add(this.ReportBtn);
            this.HeadPanel.Controls.Add(this.LoadPeopleBtn);
            this.HeadPanel.Controls.Add(this.StateLbl);
            this.HeadPanel.Controls.Add(this.StateCmb);
            this.HeadPanel.Controls.Add(this.GenderLbl);
            this.HeadPanel.Controls.Add(this.GenBtn);
            this.HeadPanel.Controls.Add(this.GenderCmb);
            this.HeadPanel.Location = new System.Drawing.Point(0, 1);
            this.HeadPanel.Name = "HeadPanel";
            this.HeadPanel.Size = new System.Drawing.Size(1143, 73);
            this.HeadPanel.TabIndex = 0;
            // 
            // NameSetLbl
            // 
            this.NameSetLbl.AutoSize = true;
            this.NameSetLbl.Location = new System.Drawing.Point(285, 24);
            this.NameSetLbl.Name = "NameSetLbl";
            this.NameSetLbl.Size = new System.Drawing.Size(92, 13);
            this.NameSetLbl.TabIndex = 13;
            this.NameSetLbl.Text = "Национальность";
            // 
            // NameSetCmb
            // 
            this.NameSetCmb.FormattingEnabled = true;
            this.NameSetCmb.Location = new System.Drawing.Point(288, 40);
            this.NameSetCmb.Name = "NameSetCmb";
            this.NameSetCmb.Size = new System.Drawing.Size(121, 21);
            this.NameSetCmb.TabIndex = 12;
            // 
            // GenParamsLbl
            // 
            this.GenParamsLbl.AutoSize = true;
            this.GenParamsLbl.Location = new System.Drawing.Point(10, 5);
            this.GenParamsLbl.Name = "GenParamsLbl";
            this.GenParamsLbl.Size = new System.Drawing.Size(143, 13);
            this.GenParamsLbl.TabIndex = 11;
            this.GenParamsLbl.Text = "Параметры для генерации";
            // 
            // ReportBtn
            // 
            this.ReportBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ReportBtn.Location = new System.Drawing.Point(1029, 38);
            this.ReportBtn.Name = "ReportBtn";
            this.ReportBtn.Size = new System.Drawing.Size(102, 23);
            this.ReportBtn.TabIndex = 10;
            this.ReportBtn.Text = "Отчет";
            this.ReportBtn.UseVisualStyleBackColor = true;
            this.ReportBtn.Click += new System.EventHandler(this.ReportBtn_Click);
            // 
            // LoadPeopleBtn
            // 
            this.LoadPeopleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LoadPeopleBtn.Location = new System.Drawing.Point(921, 38);
            this.LoadPeopleBtn.Name = "LoadPeopleBtn";
            this.LoadPeopleBtn.Size = new System.Drawing.Size(102, 23);
            this.LoadPeopleBtn.TabIndex = 10;
            this.LoadPeopleBtn.Text = "Загрузить из БД";
            this.LoadPeopleBtn.UseVisualStyleBackColor = true;
            this.LoadPeopleBtn.Click += new System.EventHandler(this.LoadPeopleBtn_Click);
            // 
            // StateLbl
            // 
            this.StateLbl.AutoSize = true;
            this.StateLbl.Location = new System.Drawing.Point(146, 24);
            this.StateLbl.Name = "StateLbl";
            this.StateLbl.Size = new System.Drawing.Size(43, 13);
            this.StateLbl.TabIndex = 6;
            this.StateLbl.Text = "Страна";
            // 
            // StateCmb
            // 
            this.StateCmb.FormattingEnabled = true;
            this.StateCmb.Location = new System.Drawing.Point(149, 40);
            this.StateCmb.Name = "StateCmb";
            this.StateCmb.Size = new System.Drawing.Size(121, 21);
            this.StateCmb.TabIndex = 5;
            // 
            // GenderLbl
            // 
            this.GenderLbl.AutoSize = true;
            this.GenderLbl.Location = new System.Drawing.Point(10, 24);
            this.GenderLbl.Name = "GenderLbl";
            this.GenderLbl.Size = new System.Drawing.Size(27, 13);
            this.GenderLbl.TabIndex = 2;
            this.GenderLbl.Text = "Пол";
            // 
            // FakePeopleGrid
            // 
            this.FakePeopleGrid.AllowUserToOrderColumns = true;
            this.FakePeopleGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FakePeopleGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.FakePeopleGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.FakePeopleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FakePeopleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.FullName,
            this.Adress,
            this.Coordinates,
            this.Phone,
            this.Birthday,
            this.Email,
            this.Company,
            this.Occupation,
            this.Height,
            this.Weight});
            this.FakePeopleGrid.Location = new System.Drawing.Point(0, 270);
            this.FakePeopleGrid.Name = "FakePeopleGrid";
            this.FakePeopleGrid.Size = new System.Drawing.Size(1143, 313);
            this.FakePeopleGrid.TabIndex = 2;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            // 
            // FullName
            // 
            this.FullName.HeaderText = "Имя";
            this.FullName.Name = "FullName";
            // 
            // Adress
            // 
            this.Adress.HeaderText = "Адрес";
            this.Adress.Name = "Adress";
            // 
            // Coordinates
            // 
            this.Coordinates.HeaderText = "Координаты";
            this.Coordinates.Name = "Coordinates";
            // 
            // Phone
            // 
            this.Phone.HeaderText = "Телефон";
            this.Phone.Name = "Phone";
            // 
            // Birthday
            // 
            this.Birthday.HeaderText = "Дата рождения";
            this.Birthday.Name = "Birthday";
            // 
            // Email
            // 
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            // 
            // Company
            // 
            this.Company.HeaderText = "Компания";
            this.Company.Name = "Company";
            // 
            // Occupation
            // 
            this.Occupation.HeaderText = "Должность";
            this.Occupation.Name = "Occupation";
            // 
            // Height
            // 
            this.Height.HeaderText = "Рост (см)";
            this.Height.Name = "Height";
            // 
            // Weight
            // 
            this.Weight.HeaderText = "Вес (кг)";
            this.Weight.Name = "Weight";
            // 
            // FakePeoplePanel
            // 
            this.FakePeoplePanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FakePeoplePanel.Controls.Add(this.OutputTxb);
            this.FakePeoplePanel.Controls.Add(this.OutParamsLbl);
            this.FakePeoplePanel.Location = new System.Drawing.Point(0, 80);
            this.FakePeoplePanel.Name = "FakePeoplePanel";
            this.FakePeoplePanel.Size = new System.Drawing.Size(1143, 186);
            this.FakePeoplePanel.TabIndex = 1;
            // 
            // OutputTxb
            // 
            this.OutputTxb.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OutputTxb.Location = new System.Drawing.Point(13, 26);
            this.OutputTxb.Multiline = true;
            this.OutputTxb.Name = "OutputTxb";
            this.OutputTxb.Size = new System.Drawing.Size(1118, 148);
            this.OutputTxb.TabIndex = 1;
            // 
            // OutParamsLbl
            // 
            this.OutParamsLbl.AutoSize = true;
            this.OutParamsLbl.Location = new System.Drawing.Point(12, 10);
            this.OutParamsLbl.Name = "OutParamsLbl";
            this.OutParamsLbl.Size = new System.Drawing.Size(119, 13);
            this.OutParamsLbl.TabIndex = 0;
            this.OutParamsLbl.Text = "Выходные параметры";
            // 
            // FakePeopleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1147, 595);
            this.Controls.Add(this.FakePeoplePanel);
            this.Controls.Add(this.FakePeopleGrid);
            this.Controls.Add(this.HeadPanel);
            this.Name = "FakePeopleForm";
            this.Text = "Fake People Parser";
            this.Shown += new System.EventHandler(this.FakePeopleForm_Shown);
            this.HeadPanel.ResumeLayout(false);
            this.HeadPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FakePeopleGrid)).EndInit();
            this.FakePeoplePanel.ResumeLayout(false);
            this.FakePeoplePanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button GenBtn;
        private System.Windows.Forms.ComboBox GenderCmb;
        private System.Windows.Forms.Panel HeadPanel;
        private System.Windows.Forms.Label StateLbl;
        private System.Windows.Forms.ComboBox StateCmb;
        private System.Windows.Forms.Label GenderLbl;
        private System.Windows.Forms.Button LoadPeopleBtn;
        private System.Windows.Forms.DataGridView FakePeopleGrid;
        private System.Windows.Forms.Panel FakePeoplePanel;
        private System.Windows.Forms.Label GenParamsLbl;
        private System.Windows.Forms.Label OutParamsLbl;
        private System.Windows.Forms.TextBox OutputTxb;
        private System.Windows.Forms.Label NameSetLbl;
        private System.Windows.Forms.ComboBox NameSetCmb;
        private System.Windows.Forms.Button ReportBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn FullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Adress;
        private System.Windows.Forms.DataGridViewTextBoxColumn Coordinates;
        private System.Windows.Forms.DataGridViewTextBoxColumn Phone;
        private System.Windows.Forms.DataGridViewTextBoxColumn Birthday;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Company;
        private System.Windows.Forms.DataGridViewTextBoxColumn Occupation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Height;
        private System.Windows.Forms.DataGridViewTextBoxColumn Weight;
    }
}

