﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using Microsoft.Office;
using Excel = Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using MySql.Data;
using MySql.Data.Entity;
using MySql.Data.MySqlClient;

namespace fakepeopleparser
{
    public partial class FakePeopleForm : Form
    {
        DBConnector connector = new DBConnector();
        public FakePeopleForm()
        {
            InitializeComponent();
        }

        private void GenBtn_Click(object sender, EventArgs e)
        {
            string gender = String.Empty;
            string state = String.Empty;
            string nameset = String.Empty;
            OutputTxb.Text = String.Empty;

            if (GenderCmb.SelectedIndex > -1)
            {
                gender = GenderCmb.SelectedItem.ToString();
            }
            else
            {
                //по умолчанию
                gender = Constants.cGenderDefault;
            }
            
            if (NameSetCmb.SelectedIndex > -1)
            {
                nameset = NameSetCmb.Text;
            }
            else
            {
                //по умолчанию
                nameset = Constants.cNamesetDefault;
            }

            if (StateCmb.SelectedIndex > -1)
            {
                state = StateCmb.SelectedItem.ToString();
            }
            else
            {
                //по умолчанию
                state = Constants.cStateDefault;
            }

            gender = Requests.FindValueInArray(Constants.gendersArr, gender);
            nameset = Requests.FindValueInArray(Constants.namesetArr, nameset);
            state = Requests.FindValueInArray(Constants.statesArr, state);

            OutputTxb.Text = Requests.CreateFakePeopleRequest(gender, nameset, state);
        }

        private void FakePeopleForm_Shown(object sender, EventArgs e)
        {
            //заполнение комбобоксов
            Requests.SetComboboxItems(GenderCmb, Constants.gendersArr);
            Requests.SetComboboxItems(StateCmb, Constants.statesArr);
            Requests.SetComboboxItems(NameSetCmb, Constants.namesetArr);

            GenderCmb.SelectedIndex = GenderCmb.Items.IndexOf(Constants.cGenderDefault);
            StateCmb.SelectedIndex = StateCmb.Items.IndexOf(Constants.cStateDefault);
            NameSetCmb.SelectedIndex = NameSetCmb.Items.IndexOf(Constants.cNamesetDefault);

            //можно добавить проверку наличия интернета - например, посылать запрос на ya.ru и смотреть, какой ответ приходит
        }

        private void LoadPeopleBtn_Click(object sender, EventArgs e)
        {
            List<string[]> data = new List<string[]>();
            int rowsCount = FakePeopleGrid.Rows.Count;
            if (rowsCount > 1)
            {
                for (int i = rowsCount; i > 1; i--)
                {
                    FakePeopleGrid.Rows.Remove(FakePeopleGrid.Rows[0]);
                };
            }
            connector.LoadAllRecordsFromDB(data);

            foreach (string[] s in data)
                FakePeopleGrid.Rows.Add(s);
        }

        private void ReportBtn_Click(object sender, EventArgs e)
        {
            Report.GetExcelReport(FakePeopleGrid);
        }
    }
}
