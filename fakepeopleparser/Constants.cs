﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fakepeopleparser
{
    class Constants
    {
        //сайт гененрирования
        public const string cGetUserUrl = "http://www.fakenamegenerator.com/gen";
        public const string cSpace = " ";
        //папка для логов
        public const string cLogsFolder = "logs";
        public const string cFormatLogFile = ".txt";
        public const string cGenderDefault = "random";
        public const string cNamesetDefault = "american";
        public const string cStateDefault = "united_states";

        static public string[,] gendersArr = new string[,] {
            {"random", "random"},
            {"male", "male"},
            {"female", "female"}
        };
        static public string[,] statesArr = new string[,] {
                {"australia", "au"},
                {"austria", "as"},
                {"belgium","bg"},
                {"brazil","br"},
                {"canada","ca"},
                {"cyprus_anglicized","cyen"},
                {"cyprus_greek", "cygk" },
                {"czech_republic","cz"} ,
                {"denmark","dk"},
                {"estonia","ee"},
                {"finland","fi"},
                {"france","fr"},
                {"germany","gr"},
                {"greenland","gl"},
                {"hungary","hu"},
                {"iceland","is"},
                {"italy","it"},
                {"netherlands","nl"},
                {"new_zealand","nz"},
                {"norway","no"},
                {"poland","pl"},
                {"portugal","pt"},
                {"slovenia","sl"},
                {"south_africa","za"},
                {"spain","sp"},
                {"sweden","sw"},
                {"switzerland","sz"},
                {"tunisia","tn"},
                {"united_kingdom","uk"},
                {"united_states","us"},
                {"uruguay","uy"}
        };
        static public string[,] namesetArr = new string[,] {
                {"american", "us"},
                {"arabic ", "ar"},
                {"australian ","au"},
                {"brazil","br"},
                {"cheche_latin ","celat"},
                {"chinese ","ch"},
                {"chinese_traditional ", "zhtw" },
                {"croatian ","hr"} ,
                {"czech ","cs"},
                {"danish ","dk"},
                {"dutch ","nl"},
                {"england ","en"},
                {"eritrean ","er"},
                {"finnish ","fi"},
                {"french ","fr"},
                {"german ","gr"},
                {"greenland ","gl"},
                {"hispanic ","sp"},
                {"hobbit ","hobbit"},
                {"hungarian ","hu"},
                {"icelandic ","is"},
                {"igbo ","ig"},
                {"italian ","it"},
                {"japanese ","jpja"},
                {"japanese_anglicized ","jp"},
                {"klingon ","tlh"},
                {"ninja ","ninja"},
                {"norwegian ","no"},
                {"persian ","fa"},
                {"polish ","pl"},
                {"russian ","ru"},
                {"russia  ","rucyr"},
                {"scottish  ","gd"},
                {"slovenian  ","sl"},
                {"swedish  ","sw"},
                {"thai  ","th"},
                {"vietnamese  ","vn"}
        };
    }
}
