﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office;
using Excel = Microsoft.Office.Interop.Excel;

namespace fakepeopleparser
{
    class Report
    {
        public static void GetExcelReport(System.Windows.Forms.DataGridView InputGrid)
        {
            Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();
            Excel.Workbook ExcelWorkBook;
            Excel.Worksheet ExcelWorkSheet;
            ExcelWorkBook = ExcelApp.Workbooks.Add(System.Reflection.Missing.Value);
            ExcelWorkSheet = (Excel.Worksheet)ExcelWorkBook.Worksheets.get_Item(1);
            ExcelWorkSheet.Cells[1, 7] = "Fake People Report";

            for (int i = 1; i < InputGrid.Columns.Count + 1; i++)
            {
                ExcelWorkSheet.Cells[2, i] = InputGrid.Columns[i - 1].HeaderText;
            }
            for (int i = 0; i < InputGrid.Rows.Count - 1; i++)
            {
                for (int j = 1; j < InputGrid.Columns.Count + 1; j++)
                {
                    if (InputGrid.Rows[i].Cells[j - 1].Value != null)
                    {
                        ExcelWorkSheet.Cells[i + 3, j] = InputGrid.Rows[i].Cells[j - 1].Value.ToString();
                    }
                }
            }
            ExcelWorkSheet.Columns.EntireColumn.AutoFit();

            ExcelApp.Visible = true;
        }
    }
}
